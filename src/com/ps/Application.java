package com.ps;

import java.time.LocalDateTime;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

	public static void main(String[] args) {
		// Sample data to cache
		Map<Integer, String> data = Collections.unmodifiableMap(Stream
				.of(new SimpleEntry<>(0, "zero"), new SimpleEntry<>(1, "one"), new SimpleEntry<>(2, "two"),
						new SimpleEntry<>(3, "three"), new SimpleEntry<>(4, "four"), new SimpleEntry<>(5, "five"),
						new SimpleEntry<>(6, "six"), new SimpleEntry<>(7, "seven"), new SimpleEntry<>(8, "eight"),
						new SimpleEntry<>(9, "nine"), new SimpleEntry<>(10, "ten"), new SimpleEntry<>(11, "eleven"),
						new SimpleEntry<>(12, "twelve"))
				.collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue())));

		// Initialize the cache and set the timeout to hold data
		SampleCache<Integer, String> sampleCache = new SampleCache<>(10);
		// Put sample data into cache
		sampleCache.putAll(data);
	}

}

class SampleCache<K, V> extends ConcurrentHashMap<K, V> {

	private static final long serialVersionUID = 1L;

	private Map<K, LocalDateTime> timeMap = new ConcurrentHashMap<>();

	private long timeout = 0;

	SampleCache(long timeout) {
		this.timeout = timeout;
		init();
	}

	void init() {
		new Thread(() -> {
			while (true) {
				for (K key : timeMap.keySet()) {

					if (LocalDateTime.now().isAfter(timeMap.get(key))) {
						System.out.println("Timeout occurred at " + LocalDateTime.now() + "| Removing key :" + key
								+ " and value :" + get(key));
						timeMap.remove(key);
						remove(key);
					}
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public V put(K key, V value) {
		System.out.println(LocalDateTime.now() + " : " + LocalDateTime.now().plusMinutes(timeout));
		timeMap.put(key, LocalDateTime.now().plusMinutes(timeout));
		return super.put(key, value);
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		for (K key : m.keySet()) {
			put(key, m.get(key));
		}
	}

	public V putIfAbsent(K key, V value) {
		if (!contains(key)) {
			return put(key, value);
		} else {
			return get(value);
		}
	}
}